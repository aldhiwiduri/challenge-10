/* eslint-disable no-undef */
// __tests__/index.test.jsx

import { render, screen } from '@testing-library/react'
import Home from '../pages/index'

// eslint-disable-next-line no-undef
describe('Home', () => {
  // eslint-disable-next-line no-undef
  it('renders a heading', () => {
    render(<Home />)

    const heading = screen.getByRole('heading', {
      name: /welcome to next\.js!/i,
    })

    // eslint-disable-next-line no-undef
    expect(heading).toBeInTheDocument()
  })
})

describe('Home', () => {
  it('renders a heading', () => {
    render(<Home />)

    const heading = screen.getByRole('heading', {
      name: /welcome to next\.js!/i,
    })

    expect(heading).toBeInTheDocument()
  })
})