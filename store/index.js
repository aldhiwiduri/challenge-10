const { createStore } = require("redux");

const initialState = {
  //   counter: 2,
  is_logged_in: false,
};

const rootReducer = (state = initialState, action) => {
  switch (action.type) {
    // case "INCREMENT": {
    //   return {
    //     ...state,
    //     counter: state.counter + 1,
    //   };
    // }
    // case "INCREMENT": {
    //   return {
    //     ...state,
    //     counter: state.counter - 1,
    //   };
    // }
    case "USER_LOGIN": {
      return {
        ...state,
        is_logged_in: true,
      };
    }
    case "USER_LOGOUT": {
      return {
        ...state,
        is_logged_in: false,
      };
    }
    default: {
      return {
        ...state,
      };
    }
  }
};

const store = createStore(rootReducer);

export default store;

store.dispatch({ type: "USER_LOGIN" });
// store.dispatch({ type: "INCREMENT" });
// console.log(store.getState());
