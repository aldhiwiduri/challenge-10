/* eslint-disable jsx-a11y/alt-text */
import { Carousel } from "react-bootstrap";
import Image from "next/image";
import Navbar from "../comps/Navbar";
import Footer from "../comps/foother";
import styles from "../styles/Home.module.css";


function Home() {
  return (
    <div className={styles.container}>

      <div className={styles.Nav}>
        <Navbar />
      </div>
      <div className={styles.logo}>
        <Image src="/logo 1.png" width={80} height={80}></Image>
      </div>
      <div className={styles.h5}>
        <h1>Welcome To Suit Game </h1>
      </div>
      <div className={styles.container1}>
        <Carousel fade>
          <Carousel.Item className={styles.Carousel_item}>
            <Image
              src="/features.jpg"
              width={1010}
              height={575}
              className="d-block w-100"
              alt="First slide"
            />
          </Carousel.Item>
          <Carousel.Item>
            <Image
              src="/rockpaperstrategy.jpg"
              width={1010}
              height={575}
              className="d-block w-100"
              alt="Second slide"
            />
          </Carousel.Item>
          <Carousel.Item>
            <Image
              src="/requirements.png"
              width={1010}
              height={575}
              className="d-block w-100"
              alt="Third slide"
            />
          </Carousel.Item>
        </Carousel>
      </div>
      <Footer />
    </div>
  );
}
export default Home;
