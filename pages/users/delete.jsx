// import React, { useState, useEffect } from "react";
// import axios from "axios";
// import { Form, Row, Col, Button, Alert } from "react-bootstrap";
// import style from "../../styles/register.module.css";

// const hapus = () => {
//   const [inputData, setInputData] = useState({
//     username: "",
//     name: "",
//     email: "",
//     password: "",
//     bornDate: "",
//   });

//   const [profileData, setprofileData] = useState(null);
//   const [isError, setIsError] = useState(false);
//   const [errMessage, setErrMessage] = useState("");

//   useEffect(() => {
//     const ENDPOINT = "https://suit-game-backend.herokuapp.com/profile";
//     const config = {
//       headers: {
//         Authorization: "Bearer " + localStorage.getItem("token"),
//       },
//     };
//     axios
//       .get(ENDPOINT, config)
//       .then((res) => {
//         setInputData((prevState) => ({
//           ...prevState,
//           username: res.data.data.username,
//           name: res.data.data.name,
//           email: res.data.data.email,
//           bornDate: res.data.data.bornDate,
//         }));
//       })
//       .catch((err) => {
//         // console.log(err.response.data.message);
//         setIsError(true);
//         setErrMessage(err.response.data.message);
//       });
//   }, []);

//   const inputhandler = (e) => {
//     setInputData((prevState) => ({
//       ...prevState,
//       [e.target.name]: e.target.value,
//     }));
//   };

//   const submitHandler = () => {
//     //request api register
//     const ENDPOINT = "https://suit-game-backend.herokuapp.com/update";
//     const config = {
//       headers: {
//         Authorization: "Bearer " + localStorage.getItem("token"),
//       },
//     };
//     axios
//       .put(
//         ENDPOINT,
//         {
//           username: inputData.username,
//           name: inputData.name,
//           email: inputData.email,
//           password: inputData.password,
//           bornDate: inputData.bornDate,
//         },
//         config
//       )
//       .then((res) => {
//         const { data } = res;
//         alert(data.message);
//         window.location.href = "/login";
//       })
//       .catch((err) => {
//         // console.log(err.response.data.message);
//         setIsError(true);
//         setErrMessage(err.response.data.message);
//         window.location.href = "/login";
//       });
//       axios
//       .delete(
//         ENDPOINT,
//         {
//           username: inputData.username,
//           name: inputData.name,
//           email: inputData.email,
//           password: inputData.password,
//           bornDate: inputData.bornDate,
//         },
//         config
//       )
//       .then((res) => {
//         const { data } = res;
//         alert(data.message);
//         window.location.href = "/login";
//       })
//       .catch((err) => {
//         // console.log(err.response.data.message);
//         setIsError(true);
//         setErrMessage(err.response.data.message);
//         window.location.href = "/login";
//       });
//   };

//   return (
//     <>
//       <div>
//         {isError ? <Alert variant="danger"> {errMessage}</Alert> : ""}
//         <h5 className={style.form_h5}> Form delete </h5>
//         <div className={style.Container}>
//           <div className={style.Container_Form}>
//             <div className="form">
//               <Form>
//                 <Form.Group
//                   className="mb-3"
//                   controlId="exampleForm.ControlInput1"
//                 >
//                   <Form.Label>User Name</Form.Label>
//                   <Form.Control
//                     type="text"
//                     name="username"
//                     value={inputData.username}
//                     onChange={inputhandler}
//                     placeholder="User Name"
//                   />
//                 </Form.Group>
//                 <Form.Group
//                   className="mb-3"
//                   controlId="exampleForm.ControlInput1"
//                 >
//                   <Form.Label>Name</Form.Label>
//                   <Form.Control
//                     type="text"
//                     name="name"
//                     value={inputData.name}
//                     onChange={inputhandler}
//                     placeholder=" Name"
//                   />
//                 </Form.Group>
//                 <Form.Group
//                   className="mb-3"
//                   controlId="exampleForm.ControlInput1"
//                 >
//                   <Form.Label>Email address</Form.Label>
//                   <Form.Control
//                     type="email"
//                     name="email"
//                     value={inputData.email}
//                     onChange={inputhandler}
//                     placeholder="name@example.com"
//                   />
//                 </Form.Group>
//                 <Form.Group
//                   as={Row}
//                   className="mb-3"
//                   controlId="exampleForm.ControlInput1"
//                 >
//                   <Form.Label column sm="3">
//                     Password
//                   </Form.Label>
//                   <Col sm="10">
//                     <Form.Control
//                       type="password"
//                       name="password"
//                       value={inputData.password}
//                       onChange={inputhandler}
//                       placeholder="Password"
//                     />
//                   </Col>
//                 </Form.Group>
//                 <Form.Group
//                   className="mb-3"
//                   controlId="exampleForm.ControlInput1"
//                 >
//                   <Form.Label>Born Date</Form.Label>
//                   <Form.Control
//                     type="date"
//                     name="bornDate"
//                     value={inputData.bornDate}
//                     onChange={inputhandler}
//                     placeholder="1999-06-15"
//                   />
//                 </Form.Group>
//                 <Button variant="primary" onClick={submitHandler}>
//                   Submit
//                 </Button>
//               </Form>
//             </div>
//           </div>
//         </div>
//       </div>
//     </>
//   );
// };

// export default hapus;


import React from 'react'

export default function Delete() {
  return (
    <div>Delete</div>
  )
}

