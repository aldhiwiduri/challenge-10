/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable @next/next/link-passhref */
import { useEffect, useState } from "react";
import Link from "next/link";
import axios from "axios";
import {Alert } from "react-bootstrap";
import style from "../../styles/register.module.css";
import { useRouter } from "next/router";
import Navbar from "../../comps/Navbar";

export default function Profile() {
  const [profileData, setprofileData] = useState(null);

  const [isError, setIsError] = useState(false);
  const [errMessage, setErrMessage] = useState("");
  const router = useRouter()
  // eslint-disable-next-line no-unused-vars
  const inputhandler = (e) => {
    // eslint-disable-next-line no-undef
    setInputData((prevState) => ({
      ...prevState,
      [e.target.name]: e.target.value,
    }));
  };
  useEffect(() => {
    const ENDPOINT = "https://suit-game-backend.herokuapp.com/profile";
    const token = localStorage.getItem("token");
    if(!token) {
      router.push('/login')
    }
    const config = {
      headers: {
        Authorization: "Bearer " + token,
      },
    };
    axios
      .get(ENDPOINT, config)
      .then((res) => {
        setprofileData(res.data.data);
      })
      .catch((err) => {
        // console.log(err.response.data.message);
        setIsError(true);
        setErrMessage(err.response.data.message);
      });
  }, []);

  return (
    <>
      <div>
        <Navbar/>
        {isError ? <Alert variant="danger"> {errMessage}</Alert> : ""}
        <h5 className={style.form_h5}> Profile users </h5>
        <div className={style.Container}>
          <div>
            <table className="table table-dark table-striped">
              <tbody>
                <tr>
                  <th scope="row">Username</th>
                  <td>{profileData && profileData.username}</td>
                </tr>
                <tr>
                  <th scope="row">email</th>
                  <td> {profileData && profileData.email}</td>
                </tr>
                <tr>
                  <th scope="row">Name</th>
                  <td> {profileData && profileData.name}</td>
                </tr>
                <tr>
                  <th scope="row">Born date</th>
                  <td>
                    {profileData && profileData.bornDate}
                  </td>
                </tr>
              </tbody>
            </table>
            <Link href="/users/edit">
              <button className="btn btn-primary">edit</button>
            </Link>
          </div>
        </div>
      </div>
    </>
  );
}
