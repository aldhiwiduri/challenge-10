/* eslint-disable @next/next/no-sync-scripts */
import React, { useState, useEffect } from "react";
import Head from 'next/head'
import Image from "next/image"
import axios from "axios";
import { Form, Row, Col, Button, Alert } from "react-bootstrap";
import style from "../../styles/register.module.css";
import Photo from "../../comps/photo"

const Edit = () => {
  const [inputData, setInputData] = useState({
    username: "",
    name: "",
    email: "",
    password: "",
    bornDate: "",
    photo:"/logo 1.png"
  });

  const [isError, setIsError] = useState(false);
  const [errMessage, setErrMessage] = useState("");

  useEffect(() => {
    const ENDPOINT = "https://suit-game-backend.herokuapp.com/profile";
    const config = {
      headers: {
        Authorization: "Bearer " + localStorage.getItem("token"),
      },
    };
    axios
      .get(ENDPOINT, config)
      .then((res) => {
        setInputData((prevState) => ({
          ...prevState,
          username: res.data.data.username,
          name: res.data.data.name,
          email: res.data.data.email,
          bornDate: res.data.data.bornDate,
          photo: res.data.data.photo ? res.data.data.photo: prevState.photo
        }));
      })
      .catch((err) => {
        // console.log(err.response.data.message);
        setIsError(true);
        setErrMessage(err.response.data.message);
      });
  },[]);

  const inputhandler = (e) => {
    setInputData((prevState) => ({
      ...prevState,
      [e.target.name]: e.target.value,
    }));
  };
  const submitHandler = () => {
    //request api register
    const ENDPOINT = "https://suit-game-backend.herokuapp.com/update";
    const config = {
      headers: {
        Authorization: "Bearer " + localStorage.getItem("token"),
      },
    };
    axios
      .put(
        ENDPOINT,
        {
          username: inputData.username,
          name: inputData.name,
          email: inputData.email,
          password: inputData.password,
          bornDate: inputData.bornDate,
        },
        config
      )
      .then((res) => {
        const { data } = res;
        alert(data.message);
        window.location.href = "/users/profile/";
      })
      .catch((err) => {
        // console.log(err.response.data.message);
        setIsError(true);
        setErrMessage(err.response.data.message);
        window.location.href = "/login";
      });
  };

  return (
    <>
      <div className={style.top}>
        <Head>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossOrigin="anonymous" />
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossOrigin="anonymous"></script>
        </Head>
        {isError ? <Alert variant="danger"> {errMessage}</Alert> : ""}
        <div className={style.Container}>
          <div className={style.Container_Form}>
            <div className={style.photo}>
            <Image className={style.photo_img} src={inputData.photo} width={150} height={150} alt="photo profile"/>
            </div>       
            <button type="button" className="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
              edit photo profile
            </button>

            <div className="modal fade" id="exampleModal" tabIndex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div className="modal-dialog">
                <div className="modal-content">
                  <div className="modal-header">
                    <h5 className="modal-title" id="exampleModalLabel">edit photo profile</h5>
                    <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                  </div>
                  <div className="modal-body">
                    <Photo/>
                  </div>
                  <div className="modal-footer">
                    <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="button" className="btn btn-primary">Save changes</button>
                  </div>
                </div>
              </div>
            </div>
            <div className="form">
            <h5 className={style.form_h5}> Form Update </h5>
              <Form>
                <Form.Group
                  className="mb-3"
                  controlId="exampleForm.ControlInput1"
                >
                  <Form.Label>User Name</Form.Label>
                  <Form.Control
                    type="text"
                    name="username"
                    value={inputData.username}
                    onChange={inputhandler}
                    placeholder="User Name"
                  />
                </Form.Group>
                <Form.Group
                  className="mb-3"
                  controlId="exampleForm.ControlInput1"
                >
                  <Form.Label>Name</Form.Label>
                  <Form.Control
                    type="text"
                    name="name"
                    value={inputData.name}
                    onChange={inputhandler}
                    placeholder=" Name"
                  />
                </Form.Group>
                <Form.Group
                  className="mb-3"
                  controlId="exampleForm.ControlInput1"
                >
                  <Form.Label>Email address</Form.Label>
                  <Form.Control
                    type="email"
                    name="email"
                    value={inputData.email}
                    onChange={inputhandler}
                    placeholder="name@example.com"
                  />
                </Form.Group>
                <Form.Group
                  as={Row}
                  className="mb-3"
                  controlId="exampleForm.ControlInput1"
                >
                  <Form.Label column sm="3">
                    Password
                  </Form.Label>
                  <Col sm="10">
                    <Form.Control
                      type="password"
                      name="password"
                      value={inputData.password}
                      onChange={inputhandler}
                      placeholder="Password"
                    />
                  </Col>
                </Form.Group>
                <Form.Group
                  className="mb-3"
                  controlId="exampleForm.ControlInput1"
                >
                  <Form.Label>Born Date</Form.Label>
                  <Form.Control
                    type="date"
                    name="bornDate"
                    value={inputData.bornDate}
                    onChange={inputhandler}
                    placeholder="1999-06-15"
                  />
                </Form.Group>
                <Button variant="primary" onClick={submitHandler}>
                  Submit
                </Button>
              </Form>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Edit;
