import Link from 'next/link'
import axios from "axios";
import Navbar from "../comps/Navbar";
import style from "../styles/leaderboard.module.css"
export async function getStaticProps() {
  const url = "https://suit-game-backend.herokuapp.com/leaderboard";
  //   const result = await axios.get(url);
  const {
    data: { data },
  } = await axios.get(url);
  //   console.log(result);
  return {
    props: {
      //   data: result.data,
      data,
    },
  };
}

function LeaderBoard(props) {
  const leaderboard = props.data;
  return (
    <>
    <Navbar/>
      <div className="container p-5">
        <p className={style.p}>
          <Link href="https://suit-game-backend.herokuapp.com/leaderboard/export" replace>
            <a className={style.a}>link LeaderBoard PDF</a>
          </Link>
        </p>
         <table className="table table-dark table-striped mt-5">
          <thead>
            <tr>
              <th scope="col">Rank</th>
              <th scope="col">Name</th>
              <th scope="col">Point</th>
            </tr>
          </thead>
          <tbody>
            {leaderboard &&
              leaderboard.map((item) => (
                <tr key={item.rank}>
                  <td>{item.rank}</td>
                  <td>{item.name}</td>
                  <td>{item.point}</td>
                </tr>
              ))}
          </tbody>
        </table>
      </div>
    </>
  );
}


export default LeaderBoard;