import { useEffect, useState } from "react";
import Image from "next/image";
import axios from "axios";
import style from "../styles/fights.module.css";
import { useRouter } from "next/router";

const Fights = () => {
  const [isError, setIsError] = useState(false);
  const [result, setResult] = useState(null);
  const [playerActive, setPlayerActive] = useState("");
  const [comActive, setComActive] = useState("");
  const router = useRouter()

  useEffect(() => {
    const token = localStorage.getItem("token");
    if(!token) {
      router.push('/login')
    }
  },[router])

  const submitHandler = (suit) => {
    const url = "https://suit-game-backend.herokuapp.com";
    const endpoint = `${url}/fights`;
    const token = localStorage.getItem("token");
    const config = {
      headers: {
        Authorization: "Bearer " + token
      },
    };
    axios
      .post(
        endpoint,
        {
          suit,
        },
        config
      )
      .then((response) => {
        console.log(response);
        const { data } = response.data;
        const { result } = data;
        console.log(result); // debug
        setResult(result);
        setPlayerActive(data.you);
        setComActive(data.opponent);
      })
      .catch(() => {
        setIsError(true);
      });
    // console.log(suit);
  };
  const resetHandler = () => {
    setResult("");
    setPlayerActive("");
    setComActive("");
  };
  const backPage = () => {
    router.push('/')
  }

  return (
    <section className={style.hal_1}> 
    {isError ? "ERROR": ""}

      <div className={style.logo_panah}
      onClick={() => backPage() }>
        <Image src="/_.png" width={45} height={39} alt="_" />
      </div>
      <div className={style.logo}>
        <Image src="/logo 1.png" width={90} height={90} alt="logo" />
      </div>
      <div>
        <h5>ROCK PAPER SCISSORS</h5>
      </div>
      <div className={style.playerlogo}>
        <p>PLAYER 1</p>
      </div>
      <div className={style.com}>
        <p>COM</p>
      </div>
      <div className={style.player}>
        <div className={style.batu}>
          <Image
            className={playerActive === "R" ? style.active : ""}
            onClick={() => submitHandler("R")}
            src="/batu.png"
            width={128}
            height={128}
            alt="batu"
          />
        </div>
        <div className={style.kertas}>
          <Image
            className={playerActive === "P" ? style.active : ""}
            onClick={() => submitHandler("P")}
            src="/kertas.png"
            width={128}
            height={128}
            alt="kertas"
          />
        </div>
        <div className={style.gunting}>
          <Image
            className={playerActive === "S" ? style.active : ""}
            onClick={() => submitHandler("S")}
            src="/gunting.png"
            width={128}
            height={128}
            alt="gunting"
          />
        </div>
      </div>

      <div className={style.kalkulasi}>
        {/* <p className={style.vs}>{result ? result : "VS"}</p> */}
        <p className={result ? style.hasil : style.vs}>
          {result ? result : "VS"}
        </p>
      </div>
      <div className={style.com}>
        <div className={style.player_com}>
          <div className={style.batu2}>
            <Image
              className={comActive === "R" ? style.active : ""}
              src="/batu.png"
              width={128}
              height={128}
              alt="batu"
            />
          </div>
          <div className={style.kertas2}>
            <Image
              className={comActive === "P" ? style.active : ""}
              src="/kertas.png"
              width={128}
              height={142}
              alt="kertas"
            />
          </div>
          <div className={style.gunting2}>
            <Image
              className={comActive === "S" ? style.active : ""}
              src="/gunting.png"
              width={138}
              height={138}
              alt="gunting"
            />
          </div>
        </div>
      </div>
      <div className={style.refresh}>
        <Image
          src="/refresh.png"
          width={77}
          height={71}
          alt="refresh"
          onClick={() => resetHandler()}
        />
      </div>
    </section>
  );
};

export default Fights;
