import React, { useState } from 'react'
import axios from "axios";
import Spinner from './spiner';

const Photo = () => {
  const [image, setImage] = useState(null)
  const [isloading, setIsLoading] = useState(false)

  // eslint-disable-next-line no-unused-vars
  const submitHandler = (suit) => {
    const data = new FormData()
    data.append('photo',image,image.name)
    const url = "https://suit-game-backend.herokuapp.com";
    console.log(image)
    const endpoint = `${url}/photo`;
    const token = localStorage.getItem("token");
    const headers = {
        Authorization: "Bearer " + token
    };
    setIsLoading(true)
    axios({ url: endpoint, method: 'PUT', headers, data} )
      .then((response) => {
        console.log(response)
      window.alert(response.data.message)
     })
     .finally(() => {
       setIsLoading(false)
     })
  };
  return (
    <>
      <form>
        <div className="mb-3">
          <label className="form-label">Photo</label>
          <input type="file" className="form-control" name="photo" onChange={(event) => setImage(event.target.files[0])} />
        </div>
        <button type='button' className='btn btn-primary' onClick={() => submitHandler()}> {isloading ? <Spinner/> : 'submit'} </button>
      </form>
    </>
  )
};

export default Photo