import style from "../styles/footer.module.css";

export default function Footer() {
  return (
    <div className={style.footer}>
      <h5>Copyright 2022 Binar Academy</h5>
    </div>
  );
}
