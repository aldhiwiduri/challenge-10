const spiner = () => {
    return (
<div className="spinner-border spinner-grow-sm" role="status">
  <span className="visually-hidden">Loading...</span>
</div>  
 )
}
export default spiner
