/* eslint-disable no-undef */
import Link from "next/link";
import style from "../styles/Navbar.module.css";


const Navbar = () => {
  return (
    <div className={style.navbar}>
      <div className={style.bar}>
        <ul className={style.ul}>
          <div className={style.li}>
          <li>
              <Link href="/">
                <a className={style.a}>Home</a>
              </Link>
            </li>
            <li>
              <Link href="/register">
                <a className={style.a}>Register</a>
              </Link>
            </li>
            <li>
              <Link href="/login" onClick={() => login()}>
                <a className={style.a}>Login</a>
              </Link>
            </li>
            <li>
              <Link href="/users/profile">
                <a className={style.a}>Profile</a>
              </Link>
            </li>
            <li>
              <Link href="/leaderboard">
                <a className={style.a}>leaderboard</a>
              </Link>
            </li>
          </div>
        </ul>
      </div>
    </div>
  );
};



export default Navbar;
