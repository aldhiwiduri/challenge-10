/* eslint-disable no-undef */
import React from 'react'
import { render, screen } from '@testing-library/react'
import FormRegister from '../pages/register'
import '@testing-library/jest-dom';
describe('formRegister', () => {
  it('renders a heading5', () => {
    render(<FormRegister />)

    const heading5 = screen.getByRole('heading', {
      name: /Form Register/i,
      level: 5
    })
    expect(heading5).toBeInTheDocument()
  })
})
describe("<FormRegister />", () => {
  test('render email input', () => {
    render(<FormRegister />);
 
    const inputEl = screen.getByTestId("email-input");
    expect(inputEl).toBeInTheDocument();
    expect(inputEl).toHaveAttribute("type", "email");
  });
  test('pass valid email to test email input field', () => {
    render(<FormRegister />);
 
    const inputEl = screen.getByTestId("email-input");
    userEvent.type(inputEl, "test@mail.com");
 
    expect(screen.getByTestId("email-input")).toHaveValue("test@mail.com");
    expect(screen.queryByTestId("error-msg")).not.toBeInTheDocument();
  });
 
  test('pass invalid email to test input value', () => {
    render(<FormRegister />);
 
    const inputEl = screen.getByTestId("email-input");
    userEvent.type(inputEl, "test");
 
    expect(screen.getByTestId("email-input")).toHaveValue("test");
    expect(screen.queryByTestId("error-msg")).toBeInTheDocument();
    expect(screen.queryByTestId("error-msg").textContent).toEqual("Please enter a valid email.");
  });
});
