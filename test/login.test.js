/* eslint-disable no-undef */
import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import Login  from '../pages/login';
import '@testing-library/jest-dom';
 
describe('Login', () => {
    it('renders a heading5', () => {
      render(<Login />)
  
      const heading = screen.getByRole('heading', {
        name: /Form Login/i,
        level: 5
      })
  
      expect(heading).toBeInTheDocument()
    })
  })
describe("<Login />", () => {
 
  test('display login', () => {
    render(<Login />);
 
    expect(screen.getByTestId("submit-hendler")).toHaveTextContent(inputData);
  });
 
  test('submit login', () => {
    render(<Login />);
 
    const btnSubmit = screen.getByTestId("btn-submit");
    fireEvent.click(btnSubmit);
 
    expect(screen.getByTestId("submit-hendler")).toHaveTextContent(inputData);
  });
});


